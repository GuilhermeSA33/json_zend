<?php

namespace Task\Model;

use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Text\Exception\RuntimeException;
use Task\Model\Post;
use Zend\Db\Sql\Select;

class PostTable {

    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll() {
        return $this->tableGateway->select(); //Traz todos os campos do banco.
    }

    public function ordenar() {
        $resultSet = $this->tableGateway->select(function(Select $select){
            $select->order('title ASC');
        });
        return $resultSet;
    }

    public function save(Post $post) {//Salva os daos no banco.
        $data = [
            'title' => $post->title,
            'content' => $post->content
        ];
        $id = (int) $post->id;

        if ((int) $post->id === 0) {
            $this->tableGateway->insert($data);
            return;
        }if (!$this->find($id)) {
            throw new RuntimeException(sprintf(
                    "Não foi possível encontar o ID %d", $id
            ));
        }
        $this->tableGateway->update($data, ['id' => $id]);
    }

    public function find($id) {
        $id = (int) $id;

        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();

        if (!$row) {
            throw new RuntimeException(sprintf(
                    'Dados não foram recebidos', $id
            ));
        }
        return $row;
    }

    public function delete($id) {
        $this->tableGateway->delete(['id' => (int) $id]);
    }

    public function procuraNome(Post $post) {

        $rowset = $this->tableGateway->select(['title' => $post->title]);
        $row = $rowset->current();

        if (!$row) {
            return NULL;
        } else {
            return $this->tableGateway->select(['title' => $post->title]);
        }
    }

    public function findid(Post $post) {
        $id = (int) $post->id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();

        if (!$row) {
            return NULL;
        } else {
            return $this->tableGateway->select(['id' => $id]);
        }
    }

}
