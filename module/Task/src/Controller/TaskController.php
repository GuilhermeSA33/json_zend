<?php

namespace Task\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Task\Model\PostTable;
use Task\Form\PostForm;
use Task\Model\Post;
use Zend\View\Model\ViewModel;
use Zend\Db\Sql\Select;

class TaskController extends AbstractActionController {

    private $table;

    public function __construct(PostTable $table) {
        $this->table = $table;
    }

    public function indexAction() {
        $postTable = $this->table;
        $param = $this->params()->fromRoute('.json'); //Pega o ".json" da url.
        if (!is_null($param)) {                       //Verifica se existe o parametro acima e caso exista ele irá retonar o Json caso não possua retorna a View.
            return new JsonModel(
                    $postTable->ordenar() //Retornando todos os dados da tabela
            );
        }
        
        return new ViewModel([
            'posts' => $postTable->ordenar() //Retorna a View normal.
        ]);
    }

    public function addAction() {
        $form = new PostForm(); //Criando um novo formulario.
        $form->get('submit')->setValue('Adicionar Tarefa'); //Apenas adicionando um botão com o nome desejado.
        $request = $this->getRequest(); // Pegando a requisição de outra página.
        if (!$request->isPost()) {//Verifica se a requisição não está vindo pelo Post
            return ['form' => $form]; //Caso não venha ele retornará o próprio formulário.
        }
        $form->setData($request->getPost()); //Pega todos os dados passados para o formulário.
        if (!$form->isValid()) {//Verificando se as informações estão válidas. 
            return ['form' => $form]; //Caso estaja incorreto retornaremos o form novamente. 
        }
        $post = new Post(); //Criando uma classe com array para pegar os dados do form setData.
        $post->exchangeArray($form->getData()); //Pega os dados do form e passa para o exchange array e assim conseguimos acessar os dados.
        $this->table->save($post); // Aqui será feito o comando de gravação no banco de dados.
        return $this->redirect()->toRoute('post'); //Retorno para a tela principal.
    }

    public function editAction() {
        $id = $this->params()->fromRoute('id', 0); //Comando serve para pegar o parametro da rota (no caso aqui ele vai pegar o id).
        //Caso não encontre nada ele irá assumir 0. 

        if (!$id) {//Verifica se o id existe caso não exista ele irá retornar para página principal.
            return $this->redirect()->toRoute('post');
        }

        try {
            $post = $this->table->find($id); //Ele irá buscar o id no banco e passar para a variavel post.
        } catch (Exception $ex) {
            return $this->redirect()->toRoute('post'); //Caso não encontre o id escolhido ele irá voltar para a tela inicial.
        }
        $form = new PostForm(); // Criação de um novo post.
        $form->bind($post); //
        $form->get('submit')->setAttribute('value', 'Editar Tarefa'); //Atribuindo o nome do botão.

        $request = $this->getRequest(); //Pega a requisição
        if (!$request->isPost()) {//Verifica se não foi passado pelo post
            return[
                'id' => $id,
                'form' => $form
            ];
        }
        $form->setData($request->getPost());
        if (!$form->isValid()) {//Pega do post e verifica se foi válida 
            return[
                'id' => $id,
                'form' => $form
            ];
        }
        $this->table->save($post); //Se estiver tudo correto ele irá salvar a alteração no banco de dados
        return $this->redirect()->toRoute('post'); //Redirecionamento para tela principal.
    }

    public function deleteAction() {
        $id = (int) $this->params()->fromRoute(id, 0); //Verifica se o id não é 0.

        if (!$id) {
            return $this->redirect()->toRoute('post'); //Caso não exista o id ele irá voltar para a tela principal.
        }
        $this->table->delete($id); //Caso tudo esteja correto ele irá apagar os dados.
        return $this->redirect()->toRoute('post'); //Redireciona para a tela principal.
    }

    //Esse foi uma mescla com o Add e o Edit.

    public function procurarAction() {
        $form = new PostForm();
        $form->get('title')->setLabel('Nome');
        $form->get('submit')->setValue('Buscar Nome');
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return ['form' => $form];
        }//Primeiro criamos o form para receber as informções
        $form->setData($request->getPost());
        if (!$form->isValid()) {
            return ['form' => $form];
        }//Verificamos se o form é válido.
        //Essa parte é mais para a função edit.
        $post = new Post(); //Chamamos o post.
        $post->exchangeArray($form->getData()); //Pega os dados do form e passa para o exchange array e assim conseguimos acessar os dados.
        if ($request->isPost()) { //Verificamos se o Request é ássado pelo post
            if ($this->table->procuraNome($post) === NULL) {//Fazemos apenas um tratamento de erro.
                return $this->redirect()->toRoute('post'); //Caso seja um valor que não exista será redirecionado para a tela principal
            }
            return new JsonModel($this->table->procuraNome($post)); //Caso esteja tudo correto será exibido o Json.
        }
    }

    public function detalhesAction() {//Sempre usar o mesmo nome da tela.
        $form = new PostForm();
        $form->get('title')->setLabel('Id');
        $form->get('submit')->setValue('Buscar Id');
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return ['form' => $form];
        }//Primeiro criamos o form para receber as informções
        $form->setData($request->getPost());
        if (!$form->isValid()) {
            return ['form' => $form];
        }//Verificamos se o form é válido.
        //Essa parte é mais para a função edit.
        $post = new Post(); //Chamamos o post.
        $post->exchangeArray($form->getData()); //Pega os dados do form e passa para o exchange array e assim conseguimos acessar os dados.
        if ($request->isPost()) { //Verificamos se o Request é ássado pelo post
            if ($this->table->findid($post) === NULL) {//Fazemos apenas um tratamento de erro.
                return $this->redirect()->toRoute('post'); //Caso seja um valor que não exista será redirecionado para a tela principal
            }
            return new JsonModel($this->table->findid($post)); //Caso esteja tudo correto será exibido o Json.
        }
    }

}
