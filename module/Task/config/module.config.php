<?php

namespace Task;

return[
    'controllers' => [
        'factories' => [
        #Controller\TaskController::class => InvokableFactory::class
        ]
    ],
    'router' => [
        'routes' => [
            'post' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/task[/:action[/:id]]', //Esses parametros podem funcionar de forma opcional.
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'//Constraints são as regras que irão para a URL para não alterar a página.
                    ],
                    'defaults' => [
                        'controller' => Controller\TaskController::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'post' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/task[/:action[/:id]]', //Esses parametros podem funcionar de forma opcional.
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'//Constraints são as regras que irão para a URL para não alterar a página.
                    ],
                    'defaults' => [
                        'controller' => Controller\TaskController::class,
                        'action' => 'index'
                    ]
                ]
            ],
            'post_json' => [
                'type' => 'segment',
                'options' => [
                    'route' => '/task[/:action[/:id]][:.json]', //Esses parametros podem funcionar de forma opcional.
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+'//Constraints são as regras que irão para a URL para não alterar a página.
                    ],
                    'defaults' => [
                        'controller' => Controller\TaskController::class,
                        'action' => 'index'
                    ]
                ]
            ],
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            'task' => __DIR__ . "/../view"
        ],
        'strategies'=>[
            'ViewJsonStrategy'
        ]
    ]
];
